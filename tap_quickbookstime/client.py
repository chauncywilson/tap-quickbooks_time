"""REST client handling, including quickbooksTimeStream base class."""

from __future__ import annotations

import sys
from functools import cached_property
from typing import Any, Callable, Iterable

import requests
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk import typing as th  # JSON Schema typing helpers
from singer_sdk.pagination import BaseOffsetPaginator
from singer_sdk.streams import RESTStream

from tap_quickbookstime.auth import quickbooksTimeAuthenticator

if sys.version_info >= (3, 9):
    import importlib.resources as importlib_resources
else:
    import importlib_resources

_Auth = Callable[[requests.PreparedRequest], requests.PreparedRequest]


class MyPaginator(BaseOffsetPaginator):
    def get_new_paginator(self):
        return BaseOffsetPaginator(start_value=0, page_size=200)

class quickbooksTimeStream(RESTStream):
    """quickbooksTime stream class."""

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return self.config["api_url"]

    records_jsonpath = "$[*].[*].[*]"  # Or override `parse_response`.

    # Set this value or override `get_new_paginator`.
    next_page_token_jsonpath = "$.next_page"  # noqa: S105

    @cached_property
    def authenticator(self) -> _Auth:
        url = self.config["api_url"]
        url = url + "/grant"
        return quickbooksTimeAuthenticator.create_for_stream(self, url)

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed.

        Returns:
            A dictionary of HTTP headers.
        """
        headers = {
            "Authorization": f"Bearer {self.config.get('access_token')}",
            "Content-Type": "application/json"
        }
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers
    
    def prepare_request_payload(
        self,
        context: dict | None,  # noqa: ARG002
        next_page_token: Any | None,  # noqa: ARG002, ANN401
    ) -> dict | None:
        return dict()
    
    def get_url_params(
        self,
        context: dict | None,  # noqa: ARG002
        next_page_token: Any | None,  # noqa: ANN401
    ) -> dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization.

        Args:
            context: The stream context.
            next_page_token: The next page index or value.

        Returns:
            A dictionary of URL query parameters.
        """
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key
        return params

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result records.

        Args:
            response: The HTTP ``requests.Response`` object.

        Yields:
            Each record from the source.
        """
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    def get_start_date(self):
        "Returns the start date from the config or raises an exception if not set"
        try: startDate = self.config["start_date"] 
        except: raise Exception("start_date is not set in the config")
        return startDate
            
    def get_end_date(self):
        "Returns the end date from the config or today if not set"
        try: endDate = self.config["end_date"]
        except: Exception("end_date is not set in the config")
        return endDate
        #sends a new payload with the startDate and endDate