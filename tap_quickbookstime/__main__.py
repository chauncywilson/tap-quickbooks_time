"""quickbooksTime entry point."""

from __future__ import annotations

from tap_quickbookstime.tap import TapquickbooksTime

TapquickbooksTime.cli()
