"""quickbooksTime tap class."""

from __future__ import annotations

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_quickbookstime import streams
streams.CustomFieldsStream
streams.ProjectsStream
streams.ProjectActivitiesStream
streams.CurrentTotalsStream
streams.PayrollStream
streams.PayrollByJobcodeStream
streams.ProjectReportStream
streams.ProjectEstimateStream
streams.ProjectEstimateDetailStream
streams.TimesheetsStream
streams.UsersStream


class TapquickbooksTime(Tap):
    """quickbooksTime tap class."""

    name = "tap-quickbookstime"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_url",
            th.StringType,
            default="https://rest.tsheets.com/api/v1",
            description="The default url for the API service, override if necessary",
        ),
        th.Property(
            "client_id",
            th.StringType,
            required=True,
            description="The client id for the API service",
            secret=True,
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True,
            description="The client secret for the API service",
            secret=True,
        ),
        th.Property(
            "end_date",
            th.DateTimeType,
            default="2024-02-02T00:00:00+00:00",
            description="format example: 2024-01-01T00:00:00+00:00, can be overridden to return a specific end date",
        ),
        th.Property(
            "project_id",
            th.IntegerType,
            description="Used for filtering the Project Activities Stream. If not provided, no project will be returned",
            default=2557137,
        ),
        th.Property(
            "project_ids",
            th.ArrayType(th.IntegerType),
            description="Used for filtering the Project Report Stream. If not provided, no project will be returned",
            default=[12345,54321],
        ),
        th.Property(
            "redirect_uri",
            th.StringType,
            required=True,
            default="https://hotglue.xyz/callback",
            description="The redirect uri for the hotglue service, override if necessary",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            default="2024-01-01T00:00:00+00:00",
            description="format example: 2024-01-01T00:00:00+00:00, can be overridden to return a specific start date",
        ),
        th.Property(
            "user_ids",
            th.ArrayType(th.IntegerType),
            default=[15790,72727],
            description="Used for filtering the Users Stream. If not provided, no user will be returned",
        )
    ).to_dict()

    def discover_streams(self) -> list[streams.quickbooksTimeStream]:
        """Return a list of discovered streams.

        Returns:
            A list of discovered streams.
        """
        return [
            streams.CustomFieldsStream(self),
            streams.ProjectsStream(self),
            streams.ProjectActivitiesStream(self),
            streams.CurrentTotalsStream(self),
            streams.PayrollStream(self),
            streams.PayrollByJobcodeStream(self),
            streams.ProjectReportStream(self),
            streams.ProjectEstimateStream(self),
            streams.ProjectEstimateDetailStream(self),
            streams.TimesheetsStream(self),
            streams.UsersStream(self)
        ]


if __name__ == "__main__":
    TapquickbooksTime.cli()
