"""Stream type classes for tap-quickbookstime."""

from __future__ import annotations

import sys
from typing import Any

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_quickbookstime.client import quickbooksTimeStream

if sys.version_info >= (3, 9):
    import importlib.resources as importlib_resources
else:
    import importlib_resources

class CustomFieldsStream(quickbooksTimeStream):
    """Define custom stream."""

    name = "customfields"
    path = "/customfields"
    #primary_keys: t.ClassVar[list[str]] = ["id"]
    #replication_key = "last_modified"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("active", th.BooleanType),
        th.Property("required", th.BooleanType),
        th.Property("applies_to", th.StringType),
        th.Property("type", th.StringType),
        th.Property("short_code", th.StringType),
        th.Property("regex_filter", th.StringType),
        th.Property("name", th.StringType),
        th.Property("last_modified", th.DateTimeType),
        th.Property("created", th.DateTimeType),
        th.Property("ui_preference", th.StringType),
        th.Property("required_customfields", th.ArrayType(th.AnyType)),
        th.Property("show_to_all", th.BooleanType)
    ).to_dict()

class ProjectsStream(quickbooksTimeStream):
    """Define custom stream."""

    name = "projects"
    path = "/projects"
    #primary_keys: t.ClassVar[list[str]] = ["id"]
    #replication_key = "last_modified"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType, description="The projects's system ID"),
        th.Property("jobcode_id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("status", th.StringType),
        th.Property("description", th.StringType),
        th.Property("start_date", th.DateTimeType),
        th.Property("due_date", th.DateTimeType),
        th.Property("completed_date", th.DateTimeType),
        th.Property("active", th.BooleanType),
        th.Property("last_modified", th.DateTimeType),
        th.Property("created", th.DateTimeType),
        th.Property("linked_objects", th.PropertiesList(
            th.Property("notes_read_times", th.ArrayType(th.AnyType)),
        )),
    ).to_dict()

class ProjectActivitiesStream(quickbooksTimeStream):
    name = "project_activities"
    path = "/project_activities"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("user_id", th.IntegerType),
        th.Property("activity_type", th.StringType),
        th.Property("active", th.BooleanType),
        th.Property("project_id", th.IntegerType),
        th.Property("created", th.DateTimeType),
        th.Property("last_modified", th.DateTimeType),
        th.Property("project_activity_metadata", th.ArrayType(th.AnyType)),
        th.Property("linked_objects", th.PropertiesList(
            th.Property("project_notes", th.ArrayType(th.AnyType)),
        )),
    ).to_dict()

    def get_url_params(self, context: dict | None, next_page_token: Any | None) -> dict[str, Any]:
        params = super().get_url_params(context, next_page_token)
        params["project_id"] = self.config["project_id"]
        return params

class TimesheetsStream(quickbooksTimeStream):
    name = "timesheets"
    path = "/timesheets"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("user_id", th.IntegerType),
        th.Property("jobcode_id", th.IntegerType),
        th.Property("start", th.DateTimeType),
        th.Property("end", th.DateTimeType),
        th.Property("duration", th.IntegerType),
        th.Property("date", th.DateTimeType),
        th.Property("tz", th.IntegerType),
        th.Property("tz_str", th.StringType),
        th.Property("type", th.StringType),
        th.Property("location", th.StringType),
        th.Property("on_the_clock", th.BooleanType),
        th.Property("locked", th.BooleanType),
        th.Property("notes", th.StringType),
        th.Property("customfields", th.PropertiesList()),
        th.Property("attached_files", th.ArrayType(th.IntegerType)),
        th.Property("last_modified", th.DateTimeType),
    ).to_dict()

    def get_url_params(self, context: dict | None, next_page_token: Any | None) -> dict[str, Any]:
        params = super().get_url_params(context, next_page_token)
        params["modified_since"] = quickbooksTimeStream.get_end_date(self)
        return params

class UsersStream(quickbooksTimeStream):
    name = "users"
    path = "/users"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("first_name", th.StringType),
        th.Property("last_name", th.StringType),
        th.Property("group_id", th.IntegerType),
        th.Property("active", th.BooleanType),
        th.Property("employee_number", th.IntegerType),
        th.Property("salaried", th.BooleanType),
        th.Property("exempt", th.BooleanType),
        th.Property("email", th.StringType),
        th.Property("email_verified", th.BooleanType),
        th.Property("payroll_id", th.StringType),
        th.Property("mobile_number", th.StringType),
        th.Property("hire_date", th.DateTimeType),
        th.Property("term_date", th.DateTimeType),
        th.Property("last_modified", th.DateTimeType),
        th.Property("last_active", th.DateTimeType),
        th.Property("created", th.DateTimeType),
        th.Property("company_name", th.StringType),
        th.Property("dislplay_name", th.StringType),
        th.Property("submitted_to", th.DateTimeType),
        th.Property("approved_to", th.DateTimeType),
        th.Property("manager_of_group_ids", th.ArrayType(th.AnyType)),
        th.Property("pay_rate", th.IntegerType),
        th.Property("pay_interval", th.StringType),
        th.Property("customfields", th.StringType),
    ).to_dict()

class CurrentTotalsStream(quickbooksTimeStream):
    name = "current_totals"
    path = "/reports/current_totals"
    rest_method = "POST"
    schema = th.PropertiesList(
        th.Property("user_id", th.IntegerType),
        th.Property("on_the_clock", th.BooleanType),
        th.Property("timesheet_id", th.IntegerType),
        th.Property("jobcode_id", th.IntegerType),
        th.Property("group_id", th.IntegerType),
        th.Property("shift_geolocations_available", th.BooleanType),
        th.Property("shift_seconds", th.IntegerType),
        th.Property("day_seconds", th.IntegerType),
    ).to_dict()

    def prepare_request_payload(self, context, next_page_token):
        payload = super().prepare_request_payload(context, next_page_token)
        payload["data"] = {
            "on_the_clock": "both"
            }
        return payload

class PayrollStream(quickbooksTimeStream):
    name = "payroll"
    path = "/reports/payroll"
    rest_method = "POST"
    schema = th.PropertiesList(
        th.Property("user_id", th.IntegerType),
        th.Property("client_id", th.StringType),
        th.Property("start_date", th.DateTimeType),
        th.Property("end_date", th.DateTimeType),
        th.Property("total_re_seconds", th.IntegerType),
        th.Property("total_ot_seconds", th.IntegerType),
        th.Property("total_dt_seconds", th.IntegerType),
        th.Property("total_pto_seconds", th.IntegerType),
        th.Property("total_work_seconds", th.IntegerType),
    ).to_dict()

    def prepare_request_payload(self, context, next_page_token):
        payload = super().prepare_request_payload(context, next_page_token)
        payload["data"] = {
            "user_ids": self.config["user_ids"],
            "start_date": quickbooksTimeStream.get_start_date(self),
            "end_date": quickbooksTimeStream.get_end_date(self),
            "advanced_overtime": "no"
            }
        return payload

class PayrollByJobcodeStream(quickbooksTimeStream):
    name = "payroll_by_jobcode"
    path = "/reports/payroll_by_jobcode"
    rest_method = "POST"
    schema = th.PropertiesList(
        th.Property("jobcode_id", th.IntegerType),
        th.Property("total_re_seconds", th.IntegerType),
        th.Property("total_ot_seconds", th.IntegerType),
        th.Property("total_dt_seconds", th.IntegerType),
        th.Property("total_pto_seconds", th.IntegerType),
        th.Property("total_work_seconds", th.IntegerType),
    ).to_dict()

    def prepare_request_payload(self, context, next_page_token):
        payload = super().prepare_request_payload(context, next_page_token)
        payload["data"] = {
            "user_ids": "all",
            "start_date": quickbooksTimeStream.get_start_date(self),
            "end_date": quickbooksTimeStream.get_end_date(self),
            "advanced_overtime": "no"
            }
        return payload

class ProjectReportStream(quickbooksTimeStream):
    name = "project_report"
    path = "/reports/project"
    rest_method = "POST"
    schema = th.PropertiesList(
        th.Property("start_date", th.DateTimeType),
        th.Property("end_date", th.DateTimeType),
        th.Property("totals", th.PropertiesList(
            th.Property("users", th.AnyType),
            th.Property("groups", th.AnyType),
            th.Property("jobcodes", th.AnyType),
            th.Property("customfields", th.AnyType),
        ))
    ).to_dict()

    def prepare_request_payload(self, context, next_page_token):
        payload = super().prepare_request_payload(context, next_page_token)
        payload["data"] = {
            "start_date": quickbooksTimeStream.get_start_date(self),
            "end_date": quickbooksTimeStream.get_end_date(self),
            "customfielditems": "all",
            "jobcode_ids": "all"
        }

class ProjectEstimateStream(quickbooksTimeStream):
    name = "project_estimate"
    path = "/reports/project_estimate"
    rest_method = "POST"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("jobcode_id", th.IntegerType),
        th.Property("parent_jobcode_id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("status", th.StringType),
        th.Property("description", th.StringType),
        th.Property("start_date", th.DateTimeType),
        th.Property("due_date", th.DateTimeType),
        th.Property("completed_date", th.DateTimeType),
        th.Property("active", th.IntegerType),
        th.Property("last_modified", th.DateTimeType),
        th.Property("created", th.DateTimeType),
        th.Property("estimate_id", th.IntegerType),
        th.Property("estimate_by", th.StringType),
        th.Property("estimate_by__id", th.IntegerType),
        th.Property("total_clocked_in_seconds", th.IntegerType),
        th.Property("total_clocked_out_seconds", th.IntegerType),
        th.Property("total_tracked_seconds", th.IntegerType),
        th.Property("total_estimated_seconds", th.IntegerType),
        th.Property("total_note_count", th.IntegerType),
        th.Property("unread_posts_count", th.IntegerType),
        th.Property("unread_replies_count", th.IntegerType),
        th.Property("budget_progress", th.IntegerType),
        th.Property("clocked_in_user_ids", th.ArrayType(th.IntegerType)),
    ).to_dict()

    def prepare_request_payload(self, context, next_page_token):
        payload = super().prepare_request_payload(context, next_page_token)
        payload["data"] = {
            "project_ids": self.config["project_ids"],
            "active": "yes",
            "page": next_page_token,
            "limit": 200,
            "supplemental_data": "no",
            "results_as_array": "no"
        }
        return payload

class ProjectEstimateDetailStream(quickbooksTimeStream):
    name = "project_estimate_detail"
    path = "/reports/project_estimate_detail"
    rest_method = "POST"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("jobcode_id", th.IntegerType),
        th.Property("parent_jobcode_id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("status", th.StringType),
        th.Property("description", th.StringType),
        th.Property("start_date", th.DateTimeType),
        th.Property("due_date", th.DateTimeType),
        th.Property("completed_date", th.DateTimeType),
        th.Property("active", th.IntegerType),
        th.Property("last_modified", th.DateTimeType),
        th.Property("created", th.DateTimeType),
        th.Property("estimate_id", th.IntegerType),
        th.Property("estimate_by", th.StringType),
        th.Property("estimate_by__id", th.IntegerType),
        th.Property("total_clocked_in_seconds", th.IntegerType),
        th.Property("total_clocked_out_seconds", th.IntegerType),
        th.Property("total_tracked_seconds", th.IntegerType),
        th.Property("total_estimated_seconds", th.IntegerType),
        th.Property("total_note_count", th.IntegerType),
        th.Property("unread_posts_count", th.IntegerType),
        th.Property("budget_progress", th.IntegerType),
        th.Property("estimate_items", th.ArrayType(th.AnyType)),
        th.Property("on_the_clock", th.BooleanType),
        th.Property("customfielditem_id", th.IntegerType),
        th.Property("assigned_to_all", th.BooleanType),
        th.Property("preview_assigned_user_ids", th.ArrayType(th.IntegerType)),
        th.Property("total_assigned_user", th.IntegerType),
    ).to_dict()

    def prepare_request_payload(self, context, next_page_token):
        payload = super().prepare_request_payload(context, next_page_token)
        payload["data"] = {
            "project_id": self.config["project_id"],
            "supplemental_data": "yes",
            "results_as_array": "no",
        }
        return payload

# End of Reports Streams