"""quickbooksTime Authentication."""

from __future__ import annotations
from singer_sdk.authenticators import OAuthAuthenticator, SingletonMeta

# The SingletonMeta metaclass makes your streams reuse the same authenticator instance.
# If this behaviour interferes with your use-case, you can remove the metaclass.
class quickbooksTimeAuthenticator(OAuthAuthenticator, metaclass=SingletonMeta):
    """Authenticator class for quickbooksTime."""

    @property
    def oauth_request_body(self) -> dict:
        """Runs OpenID Connect to get the code to use for the OAuth request body.
        
        Define the OAuth request body for the AutomaticTestTap API.

        Returns:
            A dict with the request body
        """
        return {
            "grant_type": "authorization_code",
            "client_id": self.config["client_id"],
            "client_secret": self.config["client_secret"],
            "code": self.config.get("code"),
            "redirect_uri": self.config["redirect_uri"]
        }

    @classmethod
    def create_for_stream(cls, stream, url) -> quickbooksTimeAuthenticator:  # noqa: ANN001
        """Instantiate an authenticator for a specific Singer stream.

        Args:
            stream: The Singer stream instance.

        Returns:
            A new authenticator.
        """
        return cls(
            stream=stream,
            auth_endpoint=url,
            oauth_scopes="", # No scopes are required for Quickbooks Time API at this time
        )