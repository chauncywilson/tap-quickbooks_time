# tap-quickbookstime

`tap-quickbookstime` is a Singer tap for quickbooksTime.

Built with the [Meltano Tap SDK](https://sdk.meltano.com) for Singer Taps..

## Installation

Install from PyPi:

```bash
pipx install tap-quickbookstime
```

Install from GitHub:

```bash
pipx install git+https://github.com/ORG_NAME/tap-quickbookstime.git@main
```

## Configuration

### Accepted Config Options

## Capabilities

* `catalog`
* `state`
* `discover`
* `about`
* `stream-maps`
* `schema-flattening`
* `batch`

## Settings

| Setting             | Required | Default | Description |
|:--------------------|:--------:|:-------:|:------------|
| api_url             | False    | https://rest.tsheets.com/api/v1 | The default url for the API service, override if necessary |
| client_id           | True     | None    | The client id for the API service |
| client_secret       | True     | None    | The client secret for the API service |
| end_date            | False    | 2024-02-02T00:00:00+00:00 | format example: 2024-01-01T00:00:00+00:00, can be overridden to return a specific end date |
| project_id          | False    | 2557137 | Used for filtering the Project Activities Stream. If not provided, no project will be returned |
| project_ids         | False    | 12345, 54321 | Used for filtering the Project Report Stream. If not provided, no project will be returned |  
| redirect_uri        | True     | https://hotglue.xyz/callback | The redirect uri for the hotglue service, override if necessary |        
| start_date          | False    | 2024-01-01T00:00:00+00:00 | format example: 2024-01-01T00:00:00+00:00, can be overridden to return a specific start date |
| user_ids            | False    | 15790, 17263 | Used for filtering the Users Stream. If not provided, no user will be returned |
| stream_maps         | False    | None    | Config object for stream maps capability. For more information check out [Stream Maps](https://sdk.meltano.com/en/latest/stream_maps.html). |
| stream_map_config   | False    | None    | User-defined config values to be used within map expressions. |
| flattening_enabled  | False    | None    | 'True' to enable schema flattening and automatically expand nested properties. |
| flattening_max_depth| False    | None    | The max depth to flatten schemas. |
| batch_config        | False    | None    |             |

A full list of supported settings and capabilities is available by running: `tap-quickbookstime --about`

Most values that contain a default exist to keep the code running if a value type is not provided

## Supported Python Versions

* 3.8
* 3.9
* 3.10
* 3.11
* 3.12
* 3.13
* 3.14
* 3.15
* 3.16

A full list of supported settings and capabilities for this
tap is available by running:

```bash
tap-quickbookstime --about
```

### Configure using environment variables

This Singer tap will automatically import any environment variables within the working directory's
`.env` if the `--config=ENV` is provided, such that config values will be considered if a matching
environment variable is set either in the terminal context or in the `.env` file.

### Source Authentication and Authorization

You will need the following data types provided by Quickbooks Time

```
client_id: "sample0987654321asdfgjkl"
client_secret: "sample1234567890qwertyuiop"
```

## Usage

You can easily run `tap-quickbookstime` by itself or in a pipeline using [Meltano](https://meltano.com/).

### Executing the Tap Directly

```bash
tap-quickbookstime --version
tap-quickbookstime --help
tap-quickbookstime --config CONFIG --discover > ./catalog.json
```

## Developer Resources

Follow these instructions to contribute to this project.

### Initialize your Development Environment

```bash
pipx install poetry
poetry install
```

### Create and Run Tests

Create tests within the `tests` subfolder and
  then run:

```bash
poetry run pytest
```

You can also test the `tap-quickbookstime` CLI interface directly using `poetry run`:

```bash
poetry run tap-quickbookstime --help
```

### Testing with [Meltano](https://www.meltano.com)

_**Note:** This tap will work in any Singer environment and does not require Meltano.
Examples here are for convenience and to streamline end-to-end orchestration scenarios._

Next, install Meltano (if you haven't already) and any needed plugins:

```bash
# Install meltano
pipx install meltano
# Initialize meltano within this directory
cd tap-quickbookstime
meltano install
```

Now you can test and orchestrate using Meltano:

```bash
# Test invocation:
meltano invoke tap-quickbookstime --version
# OR run a test `elt` pipeline:
meltano elt tap-quickbookstime target-jsonl
```

### SDK Dev Guide

See the [dev guide](https://sdk.meltano.com/en/latest/dev_guide.html) for more instructions on how to use the SDK to
develop your own taps and targets.
