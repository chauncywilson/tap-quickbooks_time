"""Tests standard tap features using the built-in SDK tests library."""

from singer_sdk.testing import get_tap_test_class

from tap_quickbookstime.tap import TapquickbooksTime

SAMPLE_CONFIG = {
    "client_id": "<CLIENT_ID>",
    "client_secret": "<CLIENT_SECRET>",
}


# Run standard built-in tap tests from the SDK:
TestTapquickbooksTime = get_tap_test_class(
    tap_class=TapquickbooksTime,
    config=SAMPLE_CONFIG,
)